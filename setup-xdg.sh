#!/bin/sh

set -eu

# create dirs for xdg-user-dirs
mkdir -p "$HOME"/Desktop
mkdir -p "$HOME"/Documents
mkdir -p "$HOME"/Downloads
mkdir -p "$HOME"/Music
mkdir -p "$HOME"/Pictures
mkdir -p "$HOME"/Public
mkdir -p "$HOME"/Templates
mkdir -p "$HOME"/Videos

# set xdg-user-dirs
xdg-user-dirs-update --set DESKTOP "$(realpath "$HOME"/Desktop)"
xdg-user-dirs-update --set DOCUMENTS "$(realpath "$HOME"/Documents)"
xdg-user-dirs-update --set DOWNLOAD "$(realpath "$HOME"/Downloads)"
xdg-user-dirs-update --set MUSIC "$(realpath "$HOME"/Music)"
xdg-user-dirs-update --set PICTURES "$(realpath "$HOME"/Pictures)"
xdg-user-dirs-update --set PUBLICSHARE "$(realpath "$HOME"/Public)"
xdg-user-dirs-update --set TEMPLATES "$(realpath "$HOME"/Templates)"
xdg-user-dirs-update --set VIDEOS "$(realpath "$HOME"/Videos)"
