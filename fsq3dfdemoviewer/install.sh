#!/bin/sh -eu

HERE="$(realpath "$(dirname "$0")")"

ln -srf "$HERE/dm_68.xml" "$HOME/.local/share/mime/"
ln -srf "$HERE/fsq3dfdemoviewer.desktop" "$HOME/.local/share/applications/"

update-mime-database "$HOME/.local/share/mime"
update-desktop-database "$HOME/.local/share/applications"
