#!/bin/sh

set -eu

HERE="$(realpath "$(dirname "$0")")"

"$HERE/setup-xdg.sh"

ln -srf "$HERE/.xinitrc" "$HOME/.xinitrc"
ln -srf "$HERE/.profile" "$HOME/.profile"

if [ -d "$HOME/.config/zsh" ]
then
	ln -srf "$HOME/.profile" "$HOME/.config/zsh/.zprofile"
fi

if ! [ -e "$HOME/bin" ]
then
	# ~/bin doesn't exist, create symlink
	ln -sr "$HERE/bin" "$HOME/bin"
else
	# ~/bin file exists
	if [ -h "$HOME/bin" ]
	then
		# it's a symlink, ensure where it links to by rebirth
		rm "$HOME/bin"
		ln -sr "$HERE/bin" "$HOME/bin"
	else
		# not a symlink
		if [ -d "$HOME/bin" ]
		then
			# it's a directory, attempt to yeet if empty
			rmdir "$HOME/bin"
			ln -sr "$HERE/bin" "$HOME/bin"
		else
			# it's a file..? yeet
			rm "$HOME/bin"
			ln -sr "$HERE/bin" "$HOME/bin"
		fi
	fi
fi

echo "Updated homefolder dotfile links and ~/bin/ symlink"
