#!/bin/sh

set -eu

# pm: Package Manager abstraction

if [ -z "${distro-}" ]
then
	if [ -e /etc/os-release ]
	then
		distro="$(grep '^ID=' /etc/os-release | cut -d '=' -f '2')"
		export distro

	elif [ -n "${TERMUX_APP_PID-}" ]
	then
		export distro="termux"
	else
		echo "$0: unknown distro"
		export distro="unknown"
	fi
fi

pm_help() {
	echo "$0: read this script and give valid argument(s)"
	echo "        show pkgname(s)"
	echo "        search pkgname"
	echo "        list pkgname(s)"
	echo "        query pkgname(s)"
	echo "        depends pkgname"
	echo "        optdepends pkgname"
	echo "        conflicts pkgname"
	echo "        provides pkgname"
	echo "        file-find regex"
	echo "        file-list pkgname"
	echo "        file-update"
	echo "        install pkgname(s)"
	echo "        reinstall pkgname(s)"
	echo "        remove pkgname(s)"
	echo "        purge pkgname(s)"
	echo "        orphans"
	echo "        update"
	echo "        help|--help|-h"
}

if [ -z "${1-}" ]
then
	pm_help "$@"
	exit 1
fi

cmd="$1"
shift

# {{{ Command functions

pm_show() {
	case "$distro" in
		arch|garuda)
			# pacman -Si
			pacman --sync --info "$@"
			;;
		debian|ubuntu)
			apt show "$@"
			;;
		termux)
			apt show "$@"
			;;
	esac
}

pm_search() {
	case "$distro" in
		arch|garuda)
			# pacman -Ss
			pacman --sync --search "$@"
			;;
		debian|ubuntu)
			apt search "$@"
			;;
		termux)
			apt search "$@"
			;;
	esac
}

pm_list() {
	case "$distro" in
		arch|garuda)
			for pkg in "$@"
			do
				# for pkg in "$@"; do pacman -Ss "^$pkg\$"; done
				pacman --sync --search "^$pkg\$"
			done
			;;
		debian|ubuntu)
			apt list --installed "$@"
			;;
		termux)
			apt list --installed "$@"
			;;
	esac
}

pm_query() {
	case "$distro" in
		arch|garuda)
			pacman --query "$@"
			;;
		debian|ubuntu)
			apt list "$@"
			;;
		termux)
			apt list "$@"
			;;
	esac
}

pm_depends() {
	case "$distro" in
		arch|garuda)
			pacsift --depends "$@"
			;;
		debian|ubuntu)
			apt-cache rdepends "$@"
			;;
		termux)
			apt-cache rdepends "$@"
			;;
	esac
}

pm_optdepends() {
	case "$distro" in
		arch|garuda)
			pacsift --optdepends "$@"
			;;
		debian|ubuntu)
			echo "$0: unsupported operation on this distro"
			;;
		termux)
			echo "$0: unsupported operation on this distro"
			;;
	esac
}

pm_conflicts() {
	case "$distro" in
		arch|garuda)
			pacsift --conflicts "$@"
			;;
		debian|ubuntu)
			echo "$0: unsupported operation on this distro"
			;;
		termux)
			echo "$0: unsupported operation on this distro"
			;;
	esac
}

pm_provides() {
	case "$distro" in
		arch|garuda)
			pacsift --provides "$@"
			;;
		debian|ubuntu)
			for pkg in "$@"
			do
				apt-cache showpkg "$pkg" | sed '/Reverse Provides/,$!d'
			done
			;;
		termux)
			for pkg in "$@"
			do
				apt-cache showpkg "$pkg" | sed '/Reverse Provides/,$!d'
			done
			;;
	esac
}

pm_file_find() {
	case "$distro" in
		arch|garuda)
			# pacman -Fx
			pacman --files --regex "$@"
			;;
		debian|ubuntu)
			apt-file find --regexp "$@"
			;;
		termux)
			apt-file find --regexp "$@"
			;;
	esac
}

pm_file_list() {
	case "$distro" in
		arch|garuda)
			# pacman -Fl
			pacman --files --list "$@"
			;;
		debian|ubuntu)
			apt-file list "$@"
			;;
		termux)
			apt-file list "$@"
			;;
	esac
}

pm_file_update() {
	case "$distro" in
		arch|garuda)
			# pacman -Fy
			sudo pacman --files --refresh "$@"
			;;
		debian|ubuntu)
			apt-file update "$@"
			;;
		termux)
			apt-file update "$@"
			;;
	esac
}

pm_install() {
	case "$distro" in
		arch|garuda)
			# pacman -S --needed
			sudo pacman --sync --needed "$@"
			;;
		debian|ubuntu)
			sudo apt install "$@"
			;;
		termux)
			apt install "$@"
			;;
	esac
}

pm_reinstall() {
	case "$distro" in
		arch|garuda)
			# pacman -S --needed
			sudo pacman --sync "$@"
			;;
		debian|ubuntu)
			sudo apt reinstall "$@"
			;;
		termux)
			apt reinstall "$@"
			;;
	esac
}

pm_remove() {
	case "$distro" in
		arch|garuda)
			# pacman -Rus
			sudo pacman --remove --unneeded --recursive "$@"
			;;
		debian|ubuntu)
			sudo apt remove "$@"
			;;
		termux)
			apt remove "$@"
			;;
	esac
}

pm_purge() {
	case "$distro" in
		arch|garuda)
			# pacman -Rusn
			sudo pacman --remove --unneeded --recursive --nosave "$@"
			;;
		debian|ubuntu)
			sudo apt purge "$@"
			;;
		termux)
			apt purge "$@"
			;;
	esac
}

pm_orphans() {
	case "$distro" in
		arch|garuda)
			# Check whether there are orphaned packages to remove or not
			# This clusterfuck avoids the following unclear error:
			# "error: no targets specified (use -h for help)"
			# The following switch case sucks ass to do because pacman
			# does not tell whether it found no packages or whether it
			# had whatever error unless exit code 1 is an undocumented
			# unique exit code which I doubt...
			set +e
			# orphans="$(pacman -Qdtq)"
			orphans="$(pacman --query --deps --unrequired --quiet)"
			ret="$?"
			set -e
			case "$ret" in
				1)
					# I just fucking hope that this is an unique exit code...
					echo "No orphaned packages to remove"
					;;
				0)
					# I assume that $orphans can never be empty with 0
					# shellcheck disable=SC2086 # intentional splitting
					# pacman -Rusn $(pacman -Qdtq)
					sudo pacman --remove --unneeded --recursive --nosave $orphans
					;;
				*)
					echo "$0: 'pacman -Qdtq' returned an unknown error"
					exit 1
					;;
			esac
			;;
		debian|ubuntu)
			sudo apt autoremove --purge "$@"
			;;
		termux)
			apt autoremove --purge "$@"
			;;
	esac
}

pm_update_main() {
	case "${distro-}" in
		arch|garuda)
			if command -v informant >/dev/null 2>&1
			then
				sudo informant check
				sudo informant read
			fi

			if command -v garuda-update >/dev/null 2>&1
			then
				sudo garuda-update "$@"
			else
				# Check if Arch Linux PGP keyring is up to date
				currentver="$(LANG='' pacman -Si archlinux-keyring | grep -Po '(?<=Version         : ).*')"
				installedver="$(LANG='' pacman -Qi archlinux-keyring | grep -Po '(?<=Version         : ).*')"
				if [ "$currentver" != "$installedver" ]
				then
					echo "$0: Updating Arch Linux PGP keyring"
					sudo pacman -Sy --needed --noconfirm archlinux-keyring
				fi
			sudo pacman -Syu
			fi
			;;
		debian|ubuntu)
			sudo apt update
			apt list -u
			sudo apt upgrade
			;;
		termux)
			apt update
			apt list -u
			apt upgrade
			;;
		*)
			echo "$0: unknown distro"
			exit 1
			;;
	esac
}

pm_update() {
	# Open tmux if it's available but not open
	if [ -z "${TMUX-}" ] \
	&& command -v tmux >/dev/null 2>&1
	then
		# Re-run '$0 update' but inside tmux to enter the if's else branch
		this=$(realpath "$0")
		SHELLCUSTOMSTARTUPCOMMAND="$this update $*" tmux new-session -s "update"
	else
		# Update and purge orphans
		pm_update_main "$@"
		pm_orphans
	fi
}

# }}}

# {{{ Command selection

case "$cmd" in
	show|sh*)
		pm_show "$@"
		;;
	search|se*)
		pm_search "$@"
		;;
	list|l*)
		pm_list "$@"
		;;
	query|q*)
		pm_query "$@"
		;;
	depends|d*)
		pm_depends "$@"
		;;
	optdepends|op*)
		pm_optdepends "$@"
		;;
	conflicts|c*)
		pm_conflicts "$@"
		;;
	provides|pr*)
		pm_provides "$@"
		;;
	file-find|ff|file-f*)
		pm_file_find "$@"
		;;
	file-list|fl|file-l*)
		pm_file_list "$@"
		;;
	file-update|fu|file-u*)
		pm_file_update "$@"
		;;
	install|i*)
		pm_install "$@"
		;;
	reinstall|rei*)
		pm_reinstall "$@"
		;;
	remove|rem*)
		pm_remove "$@"
		;;
	purge|pu*)
		pm_purge "$@"
		;;
	orphans|or*)
		pm_orphans "$@"
		;;
	update|u*)
		pm_update "$@"
		;;
	--help|-h|help|h*)
		pm_help "$@"
		;;
	*)
		echo "$0: unknown command"
		pm_help "$@"
		exit 1
		;;
esac

# }}}
