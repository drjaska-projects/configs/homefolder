#!/bin/bash

set -eu

usercount=0
while IFS= read -r line
do
	if [ "$line" -gt 999 ]
	then
		if [ "$line" -gt 1999 ]
		then
			usercount=$((usercount+1))
		fi
	fi
done <<< "$(cut -d ':' -f 3 < /etc/passwd)"

if [ "$usercount" = 1 ]
then
	echo "found only 1 user on this PC"

	sudo mkdir -p /etc/systemd/system/getty@tty1.service.d/

	# [Service]
	# ExecStart=
	# ExecStart=-/sbin/agetty -o '-p -- \\u' --noclear --autologin username - $TERM

	cat <<EOF | sudo tee /etc/systemd/system/getty@tty1.service.d/autologin.conf
[Service]
ExecStart=
ExecStart=-/sbin/agetty -o '-p -f -- \\\\u' --noclear --autologin $(whoami) - \$TERM
EOF
	echo "created autologin for current user on tty1"
else
	echo "found more than 1 user on this PC"

	if [ -e /etc/systemd/system/getty@tty1.service.d/autologin.conf ]
	then
		sudo rm /etc/systemd/system/getty@tty1.service.d/autologin.conf

		echo "removed autologin for tty1"
	else
		echo "no autologin for tty1 found, nothing to remove"
	fi
fi

