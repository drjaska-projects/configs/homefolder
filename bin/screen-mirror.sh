#!/bin/bash -eu

###########################
# Screen Mirroring script #
###########################

# Author: Dr. Jaska

# Why does this exist?
# I could not manage to find a tool for minimalist Window Managers
# such as dwm or xmonad which would allow mirroring screens
# onto another screens dynamically and only when desired

#########
# Usage #
#########

# This script detects on which monitor
# the cursor currently is and mirrors that
# screen onto the monitor name given as an
# argument to this script

# If the screen detects that 2 monitors already have identical Xorg virtual
# screen real estate then it will not attempt to mirror another screen but will
# try to exec a desired default xrandr config if one hopefully exists in path:
# ~/bin/screen-mirror-restoration.sh

#################
# Example Usage #
#################

# $ ./screen-mirror.sh DP-2

# This would mirror the screen on which the cursor is onto DP-2
# meaning if cursor is on DVI-D-1 the contents of DVI-D-1
# would now be displayed on DVI-D-1 and on DP-2 where DP-2 is
# the one being mirrored to

# When re-running the script with:

# $ ./screen-mirror.sh HDMI-0

# This would not mirror the active screen to HDMI-0 but instead
# would try to execute a file located in the path
# ~/bin/screen-mirror-restoration.sh
# as it is expected to have an xrandr command which negates
# this script's --same-as flag set with the command
# xrandr --output $monitor --same-as $1

# Example of what could be in screen-mirror-restoration.sh:

#xrandr --fb $((1920+1050+1920+1920))x1680 \
#--output HDMI-0  --mode 1920x1080 --left-of DP-3    --pos 0x0         --rate 60 \
#--output DP-3    --mode 1680x1050 --left-of DP-0    --pos $((1920))x0 --rotate left \
#--output DP-0    --mode 1920x1080 --left-of DVI-D-0 --pos $((1920+1050))x0 \
#--output DVI-D-0 --mode 1920x1080                   --pos $((1920+1050+1920))x0

################
# Requirements #
################

# Xorg/X11
# 2 or more monitors
# xdotool !!!
# bash(other shells may or may not work)
# grep xrandr sed cut read



if [ "$(xrandr -q | grep -c ' connected')" = 1 ]
then
	#echo "Only a single monitor connected, aborting"
	exit 0
fi

if [ "${1-NULL}" = "NULL" ]
then
	#echo "Please provide a screen to which active screen will be mirrored"
	exit 0
fi

declare -a monitorNames=()
declare -A monitorValues

# Form a nested array of our monitors and their values(coordinates and width+height)

nested_array () {
    local array=$1 base_key=$2 values=("${@:3}")
    for i in "${!values[@]}"
    do
        eval "$1[\$base_key|$i]=\${values[i]}"
    done
}

while IFS= read -r line
do
	monitorName="$(echo "$line" | cut -d ' ' -f 1)"
	monitorNames+=("$monitorName")

	declare -a coordinates=()

	coords="$(echo "$line" | sed 's#primary ##' | cut -d ' ' -f 3)"

	width=$(echo "$coords" | cut -d 'x' -f 1)
	height=$(echo "$coords" | cut -d 'x' -f 2 | cut -d '+' -f 1)

	minX=$(echo "$coords" | cut -d '+' -f 2 | cut -d '+' -f 1)
	maxX=$(("$width" + "$minX"))
	minY=$(echo "$coords" | cut -d '+' -f 3)
	maxY=$(("$height" + "$minY"))

	coordinates+=("$minX")
	coordinates+=("$maxX")
	coordinates+=("$minY")
	coordinates+=("$maxY")

	coordinates+=("$width")
	coordinates+=("$height")

	nested_array monitorValues "$monitorName" "${coordinates[@]}"
done <<< "$(xrandr -q | grep ' connected')"

# Test if given argument is a valid monitor

for((i=0; i<${#monitorNames[@]}; i++))
do
	if [ "$1" = "${monitorNames[i]}" ]
	then
		validName="true"
	fi
done
if [ "${validName-false}" = "false" ]
then
	#echo "Enter a valid name"
	exit 0
fi

# Test if any screen is already mirrored

declare -a testMirrored=()
mirrored="false"
for line in $(xrandr -q | grep ' connected' | sed 's#primary ##'| cut -d ' ' -f 3 | cut -d '+' -f 2-)
do
	for((i=0; i<${#testMirrored[@]}; i++))
	do
		if [ "$line" = "${testMirrored[i]}" ]
		then
			mirrored="true"
			#echo "A screen is already mirrored"
			break
		fi
	done
	testMirrored+=("$line")
done

ix="|0"
ax="|1"
iy="|2"
ay="|3"

# Get mouse location

cursorCoordinatesX=$(xdotool getmouselocation | sed 's#x:##' | cut -d ' ' -f 1)
cursorCoordinatesY=$(xdotool getmouselocation | sed 's#y:##' | cut -d ' ' -f 2)

if [ "${EnableDebugPrint-0}" = 1 ]
then
	for monitorName in "${monitorNames[@]}"
	do
		echo "$monitorName"
		echo "Min X: ${monitorValues[$monitorName$ix]}"
		echo "Max X: ${monitorValues[$monitorName$ax]}"
		echo "Min Y: ${monitorValues[$monitorName$iy]}"
		echo "Max Y: ${monitorValues[$monitorName$ay]}"
		echo ""
	done
	echo "Cursor X: $cursorCoordinatesX"
	echo "Cursor Y: $cursorCoordinatesY"
fi

# Find which monitor our cursor is now on
# Screen edges are not included by choice to avoid accidents

for monitorName in "${monitorNames[@]}"
do
	if [ "$mirrored" = "true" ]
	then
		if [ -e "$HOME"/bin/screen-mirror-restoration.sh ]
		then
			bash -c "$HOME"/bin/screen-mirror-restoration.sh
		fi
		if [ -e "$HOME"/.config/feh/fehbg.sh ]
		then
			bash -c "$HOME"/.config/feh/fehbg.sh
		fi
		exit 0
	fi

	if (( cursorCoordinatesX > ${monitorValues["$monitorName$ix"]} )) && \
	   (( cursorCoordinatesX < ${monitorValues["$monitorName$ax"]} )) && \
	   (( cursorCoordinatesY > ${monitorValues["$monitorName$iy"]} )) && \
	   (( cursorCoordinatesY < ${monitorValues["$monitorName$ay"]} ))
	then
		: # Cursor found to be inside monitorName
	else
		continue # Cursor not found inside monitorName, next one
	fi

	if [ "$monitorName" != "$1" ]
	then
		xrandr --output "$1" --same-as "$monitorName" \
			--scale-from "${monitorValues["$monitorName|4"]}x${monitorValues["$monitorName|5"]}"

		if [ -e "$HOME"/.config/feh/fehbg.sh ]
		then
			bash -c "$HOME"/.config/feh/fehbg.sh
		fi
		exit 0
	else
		#echo "Instructed to mirror active screen to active screen, aborting"
		exit 0
	fi
done

