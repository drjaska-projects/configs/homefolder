#!/bin/bash

set -eu

# FZF Dir Diff

if [ "$#" != "2" ]
then
	echo "$0: Please give 2 args, the files/dirs to compare"
	exit 1
fi

# find the differing files from diff $1 $2
lines=""
while read -r line
do
	case "${line:0:1}" in
		"A")
			# Added a new file

			# remove A\t prefix
			lines+="/dev/null ${line#*$'\t'}"$'\n'
			;;
		"D")
			# Deleted a file

			# remove D\t prefix
			lines+="${line#*$'\t'} /dev/null"$'\n'
			;;
		"M")
			# Modified a file

			# remove M\t prefix
			line="${line#*$'\t'}"

			# diff 1st file and find the file in 2nd dir
			lines+="${line} $2${line#"$1"}"$'\n'
			;;
		"R")
			# Renamed a file

			# remove the R*\t prefix
			lines+="${line#*$'\t'}"$'\n'
			;;
		*)
			echo "$0: unhandled case in \`git diff --name-status\`: $line"
			exit 1
			;;
	esac
done < <(git diff --no-index --name-status "$1" "$2")

# remove trailing newline
lines="${lines%$'\n'}"

# if user does select a file then vimdiff it
while file="$(fzf --preview="git diff --no-index --color=always {1} {2}" <<< "$lines")"
do
	#shellcheck disable=2086
	vim -d $file
done
