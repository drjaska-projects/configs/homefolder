#!/bin/sh

exepath="$HOME"/quake3/oDFe.x64

if "$(echo "$1" | grep -q "\.dm_68")"
then
	if "$(echo "$1" | grep "defrag/demos")" -o "$(echo "$1" | grep -q "baseq3/demos")"
	then
		demopath="$(echo "$1" | rev | grep -o ".*/somed/" | rev | cut -d '/' -f 3-)"
	else
		if "$(echo "$1" | grep -q "file://$(hostname)")"
		then
			filepath="/$(echo "$1" | cut -d '/' -f 4-)"
		else
			filepath="$1"
		fi
		TEMPDIR="$(mktemp -dp "$(dirname "$exepath")"/defrag/demos)"
		cp "$filepath" "$TEMPDIR"
		demopath="$(echo "$TEMPDIR/$(basename "$1")" | rev | grep -o ".*/somed/" | rev | cut -d '/' -f 3-)"
	fi
else
	echo "First argument was not a file ending in .dm_68"
	exit 1
fi

"$exepath" "+demo" "$demopath"

[ -n "$TEMPDIR" ] && rm -fr "$TEMPDIR"
