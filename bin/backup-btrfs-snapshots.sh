#!/bin/bash

set -euo pipefail



#############################
# backup-btrfs-snapshots.sh #
#############################

# Author: Dr. Jaska
#
#
# TL;DR: Copy snapper btrfs snapshots
#
#     from: localhost:/mnt/btrfs/@.*/.snapshots/ID/snapshot
#
#     to: remote:/mnt/btrfs/HOSTNAME/@.*/.snapshots/ID/snapshot
#
# assuming that btrfs / is mounted on /mnt/btrfs/ on
# both and default snapper snapshot locations
# (.snapshots subvolume inside the subvolume) locally
#
#
# WARNING:
#     Filepaths which contain characters which
#     cause string splitting are not supported
#
# Example of a valid path to a snapshot:
#     /mnt/btrfs/snapshots/hostname/@home/.snapshots/1/snapshot
#
#
# notable alternatives:
#     https://github.com/baodrate/snap-sync (unmaintained)
#
#
# License: WTFPL
#
#               DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                       Version 2, December 2004
#
#     Copyright (C) 2024 Dr. Jaska
#
#     Everyone is permitted to copy and distribute verbatim or
#     modified copies of this license document, and changing
#     it is allowed as long as the name is changed.
#
#               DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#     TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#      0. You just DO WHAT THE FUCK YOU WANT TO.



######################
# base configuration #
######################

if [ "$UID" != "0" ]
then
	echo "$0: run me as root :)"
	exit 1
fi

# place snapshots from this host in dir with this name
if [ -z "${whose-}" ]
then
	#whose="$(hostname)"
	whose="$(uname -n)"
fi

# where to connect
if [ -z "${sshcmd-}" ]
then
	case "$whose" in
		paskakasa|paskakaari|paskaakaaressa|kusinenvittu|kusilammikko|saatananseveri)
			sshcmd=(ssh root@narkkinasse)
			;;
		narkkinasse)
			sshcmd=(ssh root@paskakasa)
			;;
		*)
			echo "$0: unknown host"
			exit 2
			;;
	esac
fi

localbtrfsroot="$(findmnt -n -t btrfs --list | grep -m 1 "subvol=/$" | cut -d " " -f 1)"
remotebtrfsroot="$("${sshcmd[@]}" findmnt -n -t btrfs --list | grep -m 1 "subvol=/$" | cut -d " " -f 1)"

from="$localbtrfsroot"

baseto="$remotebtrfsroot/snapshots"
to="$baseto/$whose"

remotetolocal="${to/${remotebtrfsroot}\//}"

# whether to ask to clean up snapshots or not
# the default for interactive is yes
interactive="${interactive-yes}"



#############
# utilities #
#############

# utility for cutting starting from the end
rcut() {
	cat - | rev | cut "$@" | rev
}

# utility for making sure that a remote subvolume exists or is created
exists-on-remote() {
	if [ "$#" != "1" ]
	then
		echo "misuse of $0"
		exit 3
	fi

	# check if exists or create it
	"${sshcmd[@]}" "btrfs subvolume show $1" || \
		"${sshcmd[@]}" "btrfs subvolume create $1"

	# btrfs subvolume create doesn't exit with error code in 6.6.3, double check
	# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1060380
	"${sshcmd[@]}" "btrfs -q subvolume show $1"
}

list-local-snapshots() {
	# list of local snapshots
	localsnaps="$(btrfs subvolume list -o "$from/$subvolume/.snapshots" | rcut -d " " -f 1)"
}
list-remote-snapshots() {
	# list of remote snapshots
	remotesnaps="$("${sshcmd[@]}" "btrfs subvolume list -o $to/$subvolume" | rcut -d " " -f 1)"
}

yeet-deletion-pending-snapshots() {
	for snapshot in "${todelete[@]}"
	do
		"${sshcmd[@]}" "btrfs subvolume delete $remotebtrfsroot/$snapshot"
		"${sshcmd[@]}" "rmdir $remotebtrfsroot/${snapshot%/snapshot}"
	done
}


####################
# main driver code #
####################

# create snapshots/ in remote btrfs' root if it doesn't exist
# $remotebtrfsroot/snapshots
exists-on-remote "$baseto"

# create hostname dir
# "$remotebtrfsroot/snapshots/hostname/"
"${sshcmd[@]}" "[ -d $to ] || mkdir $to"

# get all local btrfs subvolumes from btrfs partition's root which start with @
for subvolume in $(find "$from" -maxdepth 1 -type d -name "@*" | sed "s#^$from/##")
do
	# check that there can be local snapshots or skip
	[ -d "$from/$subvolume/.snapshots" ] || continue

	# check that this subvolume exist on remote or create it
	exists-on-remote "$to/$subvolume"

	list-local-snapshots

	list-remote-snapshots

	printf "Local  snapshots:\n\n%s\n\n" "$localsnaps"
	printf "Remote snapshots:\n\n%s\n\n" "$remotesnaps"

	commonsnaps=()

	# array of common snapshots
	for snapshot in $remotesnaps
	do
		if grep -q "${snapshot/${remotetolocal}\//}" <<< "$localsnaps"
		then
			commonsnaps+=("${snapshot/${remotetolocal}\//}")
		fi
	done

	# backup local snapshots of a subvolume in local btrfs root to remote
	for vol in $localsnaps
	do
		echo "$vol"

		# has it been deleted after the initial scan?
		if ! [ -e "$from/$vol" ]
		then
			echo "subvol has been deleted locally, skipping"
			list-local-snapshots
			continue
		fi

		list-remote-snapshots

		# does this snapshot exist on remote?
		if grep -q "$vol" <<< "$remotesnaps"
		then
			echo "subvol exists on remote, skipping"
			continue
		fi

		# can we use incremental sending?
		if [ "${#commonsnaps[@]}" != "0" ]
		then
			# last element in array
			latest="${commonsnaps[-1]}"
		else
			latest=""
		fi

		# create dir named after the snapshot ID
		"${sshcmd[@]}" "mkdir -p $to/${vol%snapshot}"

		# send snapshot to remote
		if [ -n "$latest" ]
		then
			echo "sending incremental"

			btrfs -q send -p "$from/$latest" "$from/$vol" | \
				"${sshcmd[@]}" "btrfs receive -e $to/${vol%snapshot}"
		else
			echo "sending non-incremental"

			btrfs -q send "$from/$vol" | \
				"${sshcmd[@]}" "btrfs receive -e $to/${vol%snapshot}"
		fi

		commonsnaps+=("$vol")

		list-remote-snapshots
	done

	# declare and init to empty
	todelete=()

	# find snapshots which only exist on remote
	for snapshot in $remotesnaps
	do
		if ! grep -q "${snapshot/${remotetolocal}\//}" <<< "$localsnaps"
		then
			todelete+=("$snapshot")
		fi
	done

	printf "Local  snapshots:\n\n%s\n\n" "$localsnaps"
	printf "Remote snapshots:\n\n%s\n\n" "$remotesnaps"

	# delete those remote-only snapshots?
	if [ "${#todelete[@]}" != "0" ]
	then
		if [ "$interactive" = "yes" ]
		then
			echo "Snapshots to delete:"
			echo
			echo "${todelete[@]}" | tr ' \t\0' '\n'
			echo
			echo "Continue? [Y/n]: "

			while read -r confirmation
			do
				case "$confirmation" in
					Y|y|""|" ")
						yeet-deletion-pending-snapshots
						break
						;;
					N|n)
						break
						;;
				esac
			done

		else # non-interactive yeet
			yeet-deletion-pending-snapshots
		fi
	fi
done
