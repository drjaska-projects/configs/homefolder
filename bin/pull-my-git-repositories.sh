#!/bin/sh

if [ -d "$HOME/xonotic/" ]
then
	XONOTICGIT="$HOME/xonotic/git"
else
	XONOTICGIT="$HOME/xonotic-git"
fi

checker () {
	if ! cd "$1" 2> /dev/null
	then
		# white - not a dir
		printf "\033[37m--- %s ---\033[0m\n" "$1"
		return
	fi

	if ! git fetch
	then
		# purple - not a configured git dir or can't connect to remote
		printf "\033[35m--- %s ---\033[0m\n" "$(pwd)"
		return
	fi

	if updates="$(git log HEAD..origin/HEAD)"
	then # log cmd succeeded with either up to date or behind
		if [ -z "$updates" ]
		then
			# do we have local changes?
			if [ -n "$(git diff-index HEAD)" ]
			then
				# blue - tracked local changes
				printf "\033[34m--- %s ---\033[0m\n" "$(pwd)"
			elif [ -n "$(git status --porcelain=v1)" ]
			then
				# yellow - untracked local changes
				printf "\033[33m--- %s ---\033[0m\n" "$(pwd)"
			else
				# green - up to date
				printf "\033[32m--- %s ---\033[0m\n" "$(pwd)"
			fi

		else
			# cyan - not up to date with origin/HEAD
			printf "\033[36m--- %s ---\033[0m\n" "$(pwd)"
			git -c color.ui=always pull --autostash
			shift # don't eval path to cwd
			if [ -n "$*" ]
			then
				# do stuff if given arguments to eval
				eval "$*"
			fi
		fi
	else
		# red - error with log HEAD..origin/HEAD
		printf "\033[31m--- %s ---\033[0m\n" "$(pwd)" > /dev/stderr
		exit 1
	fi
}

# multithreading with background daemons
# sponge absorbs all the output and prints at once
# HTTPS remotes on gitlab don't seem to have a limit
# of active connections which this would hit, SSH maybe does
threaded () {
	sleep 0.3
	( checker "$@" 2> /dev/stdout | sponge /dev/stdout ) &
}

# ~/

threaded "$XONOTICGIT"                       "./all update -l best"

# ~/programs

threaded "$HOME/programs/dwm"                "sleep 5 && sudo make clean install && make clean"

threaded "$HOME/programs/screensaver-matrix" "sleep 5 && sudo make clean install && make clean"

threaded "$HOME/programs/slock"              "sleep 5 && sudo make clean install && make clean"

# ~/.config

threaded "$HOME/.config/cargo"

threaded "$HOME/.config/distro-setup"

threaded "$HOME/.config/dunst"

threaded "$HOME/.config/feh"

threaded "$HOME/.config/fonts"

threaded "$HOME/.config/git"

threaded "$HOME/.config/gnome-terminal"

threaded "$HOME/.config/gtk-themes"

threaded "$HOME/.config/homefolder"

threaded "$HOME/.config/iamb"

threaded "$HOME/.config/keyszer"

threaded "$HOME/.config/kitty"

threaded "$HOME/.config/ncspot"

threaded "$HOME/.config/nvim"

threaded "$HOME/.config/pipewire"

threaded "$HOME/.config/quake3"

threaded "$HOME/.config/tmux"

threaded "$HOME/.config/xmobar"

threaded "$HOME/.config/xmonad"

threaded "$HOME/.config/zsh"                 "$HOME/.config/zsh/plugins/update-plugins.sh"

for repo in ~/repos/*/*/
do
	threaded "$repo"
done

[ -x "$HOME/.config/private-portal/pull-git-repositories.sh" ] \
	&& "$HOME/.config/private-portal/pull-git-repositories.sh"

wait
