#!/bin/bash

set -eu

if [ "$(lspci -k | grep -iA3 VGA | tail -n 2 | grep --count "nvidia")" != "2" ]
then
	echo "couldn't find \"nvidia\" from kernel driver in use and kernel modules"
	exit 1
fi

s="$(nvidia-settings -q CurrentMetaMode -t)"

if [[ "${s}" != "" ]]
then
	s="${s#*" :: "}"
	nvidia-settings -a CurrentMetaMode="${s//\}/, ForceCompositionPipeline=On\}}"
fi
