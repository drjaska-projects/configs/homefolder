#!/bin/bash

set -eu

# novideo fan curve adjuster

# < "$MINT" off, > "$MAXT" max, between them there is a
# 3 point curve with "$MINT", "$MIDT", "$MAXT" temperatures
# and "$MINS", "$MIDS", "$MAXS" % fan speeds

MINT=50
MIDT=70
MAXT=90

OFFS=0    # KEEP == 0
MINS=20   # KEEP >= 0, NOTE: for me fan flickers with <20
MIDS=30
MAXS=100  # KEEP <= 100

# Temp
T="$(nvidia-settings -q gpucoretemp -t)"

if [  "$T" -lt "$MINT" ]
then
	# Temp less than "$MINT", fan off
	S="$OFFS"

elif [ "$T" -gt "$MAXT" ]
then
	# Temp more than "$MAXT", cap at "$MAXS"
	S="$MAXS"

elif [ "$T" -lt "$MIDT" ]
then
	LS="$MINS"  # Low  Speed
	HS="$MIDS"  # High Speed
	LT="$MINT"  # Low  Temp
	HT="$MIDT"  # High Temp

	# lerp between A and B with Temp to get Speed
	S="$(bc -l <<< "$LS * (($T - $LT) / ($HT - $LT)) + $HS * (1 - (($T - $LT) / ($HT - $LT)))")"
else
	LS="$MIDS"  # Low  Speed
	HS="$MAXS"  # High Speed
	LT="$MIDT"  # Low  Temp
	HT="$MAXT"  # High Temp

	# lerp between A and B with Temp to get resulting Speed
	S="$(bc -l <<< "$LS * (($T - $LT) / ($HT - $LT)) + $HS * (1 - (($T - $LT) / ($HT - $LT)))")"
fi

sudo /bin/nvidia-settings \
	-a "[gpu:0]/GPUFanControlState=1" \
	-a "[fan:0]/GPUTargetFanSpeed=${S%\.*}" # truncate floats as they're "Trailing garbage"
