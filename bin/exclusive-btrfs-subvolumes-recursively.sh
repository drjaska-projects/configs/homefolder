#!/bin/sh

set -eu

# `sudo btrfs subvolume list -o` is great otherwise but
# it does not show subvolumes inside subvolumes, recursively

if [ "$#" != "1" ]
then
	echo "$0: give a path for 'btrfs subvolume list'"
	exit 1
fi

recursion() {
	# simple and easy to read oneliner which documents itself
	sudo btrfs subvolume list -o "$1" | while read -r line;	do echo "$line";recursion "$(echo "$line" | sed -e "s#.* ##" -e "s#^@[^/]*#$(findmnt -l -n -t btrfs | grep "$(echo "$line" | sed "s#/.*#]#")" | awk '{print $1}')#")";done
}

recursion "$1"
