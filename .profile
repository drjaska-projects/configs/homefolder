#!/bin/sh
# This file should be supported by dash, bash and zsh

####################################
#   Login Display Server Manager   #
####################################

############
#   HELP   #
############

# If logging into a tty which isn't an ssh one
# then prompt the user to start a Display Server
#
# Also do so automatically for specific virtual terminals

print_help() {
	echo "Login Display Server Management"
	echo
	echo "What do you wish to do?"
	echo
	echo "Start X11...: X x"
	echo "Start Wayland...: W w"
	echo "Abort and drop to TTY: N n Esc"
	echo "Poweroff: P p"
	echo "Reboot: R r"
	echo "Hibernate: H h"
	echo "Suspend: S s"
	echo "Logout: L l"
	echo
	printf "Choice: "
}

#####################
#   EARLY RETURNS   #
#####################

# Is this login from SSH, tmux, or similar
if echo "$TTY" | grep -q '/dev/pts'
then
	#echo "Welcome, SSH user"
	return
fi

# Start an SSH agent unless there already is one
if [ -z "$SSH_AUTH_SOCK" ]
then
	SSHAGENT="${SSHAGENT:-"/usr/bin/ssh-agent"}"
	if [ -x "$SSHAGENT" ]
	then
		eval "$("$SSHAGENT")"
	fi
fi

# Does this session already have a display
if [ -n "$DISPLAY" ]
then
	echo "Display already exists, Aborting Display Server startup"
	return
fi

# Are we running headless
if echo "$TTY" | grep -qv '/dev/tty'
then
	echo "Headless session detected, Aborting Display Server startup"
	return
fi

#################
#   Autologin   #
#################

# Based on Virtual Terminal NumbeR number
case "${XDG_VTNR-}" in
	"1")
		if [ -x "$(which dwm)" ]
		then
			env	wm=dwm \
				XDG_SESSION_TYPE=x11 \
				DESKTOP_SESSION=dwm \
				XDG_SESSION_DESKTOP=dwm \
				XDG_CURRENT_DESKTOP=dwm \
				startx || logout
		else
			echo "dwm unavailable"
		fi
		;;
	"2")
		if [ -x "$(which kwin_x11)" ] \
		&& [ -x "$(which startplasma-x11)" ]
		then
		env	wm=kwin \
			XDG_SESSION_TYPE=x11 \
			DESKTOP_SESSION=KDE \
			XDG_SESSION_DESKTOP=KDE \
			XDG_CURRENT_DESKTOP=KDE \
			startx || logout
		else
			echo "KDE Plasma (X11) unavailable"
		fi
		;;
	"3"|"4")
		# just present the TUI Loop directly
		;;
	*)
		# TTYs 5 to 8 straight to shell via this early return
		return
		;;
esac

##############################
#   START A DISPLAY SERVER   #
##############################

#######
# X11 #
#######

session_help_xorg() {
	echo "Which X11 Session do you want?"
	echo
	echo "dwm: D d"
	echo "KDE Plasma: K k"
	echo
	printf "Choice: "
}

session_help_kde_wm() {
	echo "Which KDE Window Manager do you want?"
	echo
	echo "dwm: D d"
	echo "KWin: K k *"
	echo
	printf "Choice: "
}

start_xorg() {
	session_help_xorg
	stty raw
	SELECTION="$(dd bs=1 count=1 2> /dev/null)"
	stty -raw
	echo

	# startx is a xinit wrapper
	# ~/.xinitrc selects WM based on DESKTOP_SESSION

	case "$SELECTION" in
		[Dd])
			if [ -x "$(which dwm)" ]
			then
				env	wm=dwm \
					XDG_SESSION_TYPE=x11 \
					DESKTOP_SESSION=dwm \
					XDG_SESSION_DESKTOP=dwm \
					XDG_CURRENT_DESKTOP=dwm \
					startx || logout
			else
				echo "dwm unavailable"
			fi
			;;
		[Kk])
			session_help_kde_wm
			stty raw
			WINDOWMANAGER="$(dd bs=1 count=1 2> /dev/null)"
			stty -raw
			echo

			case "$WINDOWMANAGER" in
				[Dd])
					if [ -x "$(which dwm)" ] \
					&& [ -x "$(which startplasma-x11)" ]
					then
							env	KDEWM=dwm \
								wm=dwm \
								XDG_SESSION_TYPE=x11 \
								DESKTOP_SESSION=KDE \
								XDG_SESSION_DESKTOP=KDE \
								XDG_CURRENT_DESKTOP=KDE \
								startx || logout
					else
						echo "KDE plasma with dwm unavailable"
					fi
					;;
				[Kk]|*)
					if [ -x "$(which kwin_x11)" ] \
					&& [ -x "$(which startplasma-x11)" ]
					then
						env	wm=kwin \
							XDG_SESSION_TYPE=x11 \
							DESKTOP_SESSION=KDE \
							XDG_SESSION_DESKTOP=KDE \
							XDG_CURRENT_DESKTOP=KDE \
							startx || logout
					else
						echo "KDE Plasma (X11) unavailable"
					fi
					;;
			esac
			;;
		*)
			echo "unknown selection"
			;;
	esac
}

###########
# Wayland #
###########

session_help_wayland() {
	echo "Which Wayland Session do you want?"
	echo
	echo "KDE Plasma: K k"
	echo
	printf "Choice: "
}

start_wayland() {
	session_help_wayland
	stty raw
	SELECTION="$(dd bs=1 count=1 2> /dev/null)"
	stty -raw
	echo

	case "$SELECTION" in
		[Kk])
			if [ -x /usr/lib/plasma-dbus-run-session-if-needed ] \
			&& [ -x /usr/bin/startplasma-wayland ]
			then
				env	XDG_SESSION_TYPE=wayland \
					DESKTOP_SESSION=KDE \
					XDG_SESSION_DESKTOP=KDE \
					XDG_CURRENT_DESKTOP=KDE \
					QT_QPA_PLATFORM=wayland \
					SDL_VIDEODRIVER=wayland \
					/usr/lib/plasma-dbus-run-session-if-needed \
						/usr/bin/startplasma-wayland || logout
			else
				echo "KDE Plasma (Wayland) unavailable"
			fi
			;;
		*)
			echo "unknown selection"
			;;
	esac
}

################
#   TUI Loop   #
################

# Ask what to do now
while true
do
	print_help
	stty raw
	REPLY="$(dd bs=1 count=1 2> /dev/null)"
	stty -raw
	echo
	case "$REPLY" in
		[Xx])
			start_xorg ;;
		[Ww])
			start_wayland ;;
		[Nn]|"$(printf '\e')")
			break ;;
		[Pp])
			systemctl poweroff ;;
		[Rr])
			systemctl reboot ;;
		[Hh])
			systemctl hibernate ;;
		[Ss])
			systemctl suspend ;;
		[Ll])
			logout ;;
		*)
			print_help ;;
	esac
done
